#################################
#        Lukas Branigan 	#
#           gs13m011		#
#Personenkennzeichen: 1310585011# 
#################################
					
                                        #####################################
                                        #GameOfLife 2.0 with OpenMP & OpenCL#
 				        #####################################
				     

#Aufbau:

Das Progamm Besteht aus 5 Klassen Gol, Grid, GridCL, GridMP und Timer.

Gol: Besteht aus Methoden zum 
	-Einlesen der Griddaten aus einem File (readfile)
	-Speichern der Griddaten in ein File (savefile)

Grid: Besteht aus Methoden zum 
	-Bekommen der Griddaten aus der Gol Klasse (getboard)
	-Setzen des ver�nderten Boards (setboard)
	-Durchlaufen des Algorithmus (virtual algo)
	-Erweitern des Grid um einen kompletten Rahmen (virtual add)
	-L�schen dieses Rahmens (remove)

GridMP: Ist eine Abgeleitete Klasse von Grid und beinhaltet 2 virtual Methoden algo und add
	-Durchlaufen des ver�nderten Algorithmus mit Parallelisierung
	-Erweitern des Grid um einen kompletten Rahmen ebenfalls mittels Parallelisierung

GridCL:	Ist ebenfalls eine Abgeleitete Klasse von Grid und beinhaltet 1 virtuelle Methode algo
	-In der Methode Algo wird OpenCl Initialisiert und die relevanten Daten f�r die Parallelisierung 
		werden an den Kernel geschickt
	-Au�erdem gibt es ein File kernel.cl welchesder den Code f�r die Verarbeitung der Daten/Grid f�r jeden Einzelnen Thread
		beinhaltet
		

	
#Funktionsweise des Programms/Algorithmus:

-Die Funktionsweise des Programms hat sich nicht wesentlich von der vorherigen Version unterschieden.
-Hinzukommen Spezieller Code f�r OpenMP und OpenCL um GameOfLife Parallelisiert ablaufen lassen zu k�nnen.

\Sequentieller Ablauf/

-Zuerst werden Daten die Daten aus den Testfiles in ein Zweidimensionales Array geschrieben.
-Es werden au�erdem die Abmessungen des Grid in 2 Variablen geschrieben
-Dieses Array, Abmessungen und die Anzahl der Generationen werden mittels der Methode getboard and 
die Grid Klasse �bergeben
-In dieser Klasse werden 2 weitere Zweidimensionale Arrays erstellt eines mit der originalen Gr��e und 
eines erweitert um einen kompletten Ring
-In der Methode algo wird eine Methode add aufgeruden welche das erweiterte Array mit den Daten in der 
Mitte bef�llt au�erdem wird die untere Zeile nach oben, die linke Zeile nach Rechts, die obere Zeile nach Unten +
und die Rechte Spalte nach Links kopiert. Am Schluss werden noch die Eckpunkte in die jeweils andere Ecke kopiert.
-In der Methode Algo werden 9 Pointer gesetzt welche innherhalb von zwei For-Schleifen immer bis zum Ende der Zeile
verschoben und dann wieder eine Zeile nach unten an den Anfang gesetzt werden. Mittels einer Variable life 
kann �berpr�ft werden wieviele der umliegenden Zellen am Leben sind.
-Tritt eine Ver�nderung einer Zelle auf wird dies wieder in das kleinere Zweidimensionale Array hineingeschrieben.
-Dies wird solange wiederholt bis alle Generationen durchlaufen wurden.
-Das fertige Array kann dann mittels setboard in die Klasse Gol verschoben werden wo dieses in ein Output File 
gespeichert wird.

-Am Schluss kommt die Timer Klasse noch zum Einsatz, welcher eine Funktion printTime hinzugef�gt wurde. Diese
gibt wenn aufgerufen mittels des Timer Objekts die Zeit auf stdout aus.
-Gemessen wurde 
	-die Zeit der Erstellung und Bef�llung des Grid in der Methode readfile (Gol)
	-die Zeit welcher der Algorithmus zur durchf�hrung der Operationen ben�tigt �ber die Methoden
	getboard, algo, add, remove und setboard (Grid)
	-die Zeit f�r das Schreiben der Griddaten in ein File und die Deallokation der Arrays im main 
	�ber die Funktion savefile

\OpenMP Ablauf/

-Einlesen und Speichern der Daten l�uft wie beim Sequentiellen Ablauf ab
-Wird durch einen Commandlineparameter --omp mitgegeben wird die Methode Algo von der Klasse GridMP �berschrieben
-Au�erdem kann mittels eines weitern Commandlineparameter --Threads die Anzahl der Cores angegeben werden
-Die neue Methode Algo beinhaltet 2 verschachtelte For-Schleifen welche durch OpenMP auf mehrere 
Threads aufgeteilt werden
-Mittels OpenMp kann man sehr schnell gewisse Teile im Code Parallelisieren
-Im Falle von GOL konnte eine Beschleunigung der Rechnenzeit fast um ein 4 Faches mit 4 Cores erreicht werden

\OpenCL Ablauf/

-Einlesen und Speichern der Daten l�uft wie beim Sequentiellen Ablauf ab
-Wird durch einen Commandlineparameter --ocl mitgegeben wird die Methode Algo von der Klasse GridCL �berschrieben
-Innerhalb der neuen Methode wird zuerst das 2Dim Array auf ein 1Dim Array umgerechnet da Probleme beim 
verarbeiten mit 2Dim Array aufgetreten sind
-Au�erdem wurde aus dem Char Array bestehend aus "." und "x" ein Int Array erstellt mit 0 und 1 (0 == "." , 1 == "x")
-Nun wurde auch schon begonnen OpenCl zu initialisieren und zu builden dies beinhaltete
	+Au�w�hlen der Platform
	+Au�w�hlen des Devices
	+Erstellen des Contexts
	+Aufrufen des Externen KernelFiles
	+Builden des Programms
	+Erstellen der Buffer
-Die n�chsten Schritte wurden dann pro Durchlauf/Generation gemacht
	+�bergeben der Daten aus dem Int Array an den Kernel (enqueueWriteBuffer)
	+Kernel Ausf�hren (enqueueNDRangeKernel)
	+Output auf Input Buffer schreiben (enqueueCopyBuffer)
-Nach dem Durchlaufen der Generationen
	+Output Buffer Daten auf Array schreiben
	+1Dim Array auf 2Dim umschreiben
	+Int Array auf Char Array umschreiben
	+Rahmen wegschneiden und auf Array Schreiben welches wieder zur�ckgegeben wird



