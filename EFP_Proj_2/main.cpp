#include <iostream>
#include <string>
#include "Gol.h"
#include "Grid.h"
#include "Timer.h"



using namespace std;

int main(int argc, char * argv[])
{
	int gen = 0;
	int mode = 0;
	int threads = 0;
	int platformId = -1;
	int deviceId = -1;
	bool measure=false;	
	char *fname = 0;
	char* sname = 0;
	int device = 0;
	Timer timer;
	Gol mGol;
	int test;
	
	/*
	//Testf�lle
	char* fname = ("test.txt");
	char* sname = ("test1.txt");
	mGol.readfile(fname,gen);
	mGol.savefile(sname);
	*/

	//Durchparsen der Commandline Argumente und je nach dem Aufrufen der Funktionen 
	if(argc < 6 && argc > 8)
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }

    if(strcmp(("--load"),argv[1])==0)
    {
	    fname = argv[2];
    }
    else
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }
                       
    if(strcmp(("--save"),argv[3])==0)
    {
		sname = argv[4];
		
	}
    else
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }

    if(strcmp(("--generations"),argv[5])==0)
    {
		gen = atoi(argv[6]);
		if(gen == 0)
		{
			cout << "Generations is 0! Generations min 1!\n";
            cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
        }
    }
    else
    {
		cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure!\n";
    }
      
	if(argc >= 8)
    {
		if(strcmp(("--measure"),argv[7])==0)
        {
			measure = true;
        }
    }
	if(argc >= 9)
	{
		if(strcmp(("--seq"),argv[8])==0)
		{
			mode = 1;
		}
		else if(strcmp(("--omp"),argv[8])==0)
		{
			mode = 2;
		}
		else if(strcmp(("--ocl"),argv[8])==0)
		{
			mode = 3;
		}
		else
		{
			cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure --seq/omp/ocl!\n";
		}
	}
	if(argc >= 10)
	{
		if(strcmp(("--threads"),argv[9])==0)
		{
			threads = atoi(argv[10]);
		}
		else if(strcmp(("--device"),argv[9])==0)
		{
			if(strcmp(("cpu"),argv[10])==0)
			{
				device = 1;
			}
			else if(strcmp(("gpu"),argv[10])==0)
			{
				device = 0;
			}
		}
		else if(strcmp(("--platformId"),argv[9])==0)
		{
			platformId = atoi(argv[10]);
		}
		else if(strcmp(("--deviceId"),argv[11])==0)
		{
			deviceId = atoi(argv[10]);
		}
		else
		{
			cout << "Usage: Gol --load FILENAME --save FILENAME --generations NUM --measure --seq/omp/ocl (--threads NUM) oder (--device cpu|gpu auto select plat/dev) oder (--platformId NUM --deviceId NUM)!\n";
		}

	}

	//Aufrufen der Readfile Funktion und �bergeben der Parameter
	mGol.readfile(fname,gen,measure,mode,threads,device,platformId,deviceId);
	
	//Savefile Funktion und Zeitmessung
	if(measure==true)
		timer.start();

	mGol.savefile(sname);

	if(measure == true)
	{
		timer.stop();
		timer.printTime(timer);
	}

	cin >> test;

    return 0;
}