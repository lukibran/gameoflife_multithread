#include "GridCL.h"
#include <ostream>
#include <stdio.h>
#include <Cl\cl.hpp>
#include <string>
#include <fstream>

using namespace std;

GridCL::GridCL(void)
{
	char* cl_board_char = NULL;
	int* cl_board_int = NULL;
}


GridCL::~GridCL(void)
{
}

void GridCL::algo()
{
	add();

	int counter=0;
	//2Dim Array in 1DIm Array umschreiben
	cl_board_char = new char[cheight* cwidth];
	for(int i=0; i<cheight; i++)
	{
		for(int j=0; j<cwidth; j++)
		{
			cl_board_char[counter] = cboard[i][j];
			counter++;
		}
	}

	//Char* Array auf Int Array �ndern
	cl_board_int = new int[cheight*cwidth];
	for(int j=0; j<cwidth*cheight; j++)
	{
		if(cl_board_char[j] == '.')
		{
			cl_board_int[j] = 0;
		}
		else if(cl_board_char[j] == 'x')
		{
			cl_board_int[j] = 1;
		}
	}

	cl::Context* context = NULL;
	
	vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	cl_context_properties cps[3] = { CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[0])(), 0 };
	
	context = new cl::Context( CL_DEVICE_TYPE_GPU, cps);
	
	vector<cl::Device> devices = context->getInfo<CL_CONTEXT_DEVICES>();

	cl::CommandQueue queue = cl::CommandQueue(*context, devices[0], 0);
	
	ifstream sourcefile("kernel.cl");
	string sourcecode( istreambuf_iterator<char>(sourcefile), (istreambuf_iterator<char>()));
	cl::Program::Sources source(1, make_pair(sourcecode.c_str(), sourcecode.length()+1));

	cl::Program program = cl::Program(*context, source);
	
	program.build(devices);

	cl::Kernel kernel(program, "gol");

	//Erstellen der Buffer
	cl::Buffer bufferinput = cl::Buffer(*context, CL_MEM_READ_ONLY, sizeof(int) * cheight * cwidth);
	cl::Buffer bufferoutput = cl::Buffer(*context, CL_MEM_WRITE_ONLY, sizeof(int) * cheight * cwidth);
	cl::Buffer bufferx = cl::Buffer(*context, CL_MEM_READ_ONLY, sizeof(int), NULL);
	cl::Buffer buffery = cl::Buffer(*context, CL_MEM_READ_ONLY, sizeof(int), NULL);

	//F�r jede Generation wird der DatenBuffer neu bef�llt und zum Kernel geschickt um die Berechnungen machen zu k�nnen
	for(int i=0;i<ggen;i++)
	{
		
		int counter=0;
		//2Dim Array in 1DIm Array umschreiben
		for(int i=0; i<cheight; i++)
		{
			for(int j=0; j<cwidth; j++)
			{
				cl_board_char[counter] = cboard[i][j];
				counter++;
			}
		}

		//Char* Array auf Int Array �ndern
		for(int j=0; j<cwidth*cheight; j++)
		{
			if(cl_board_char[j] == '.')
			{
				cl_board_int[j] = 0;
			}
			else if(cl_board_char[j] == 'x')
			{
				cl_board_int[j] = 1;
			}
		}

		//Hier werden die Buffer mit den an den Kernel �bergebenen Daten (Board,H�he,Breite) bef�llt
		queue.enqueueWriteBuffer(bufferinput, CL_TRUE, 0, sizeof(int) * cheight * cwidth, cl_board_int);
		queue.enqueueWriteBuffer(bufferx, CL_TRUE, 0, sizeof(int), &cheight);	
		queue.enqueueWriteBuffer(buffery, CL_TRUE, 0, sizeof(int), &cwidth);
		
		kernel.setArg(0, bufferinput);
		kernel.setArg(1, bufferoutput);
		kernel.setArg(2, bufferx);
		kernel.setArg(3, buffery);
	
		cl::NDRange global(cheight * cwidth);
		cl::NDRange local(50);
		//F�hrt den Kernel aus
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, global, local);

		//Nach jedem Durchlauf wird der Outputbuffer wieder auf den Inputbuffer geschrieben
		queue.enqueueCopyBuffer(bufferoutput, bufferinput, 0, 0, sizeof(int) * cheight * cwidth);

		//Hier wird die add Funktion aufgerufen um das vergr��erte Array zu bauen
		add();		
	}

	//Inhalt Buffer wird wieder in richtiges Array geschrieben schreiben
	queue.enqueueReadBuffer(bufferoutput, CL_TRUE, 0, sizeof(int) * cheight * cwidth, cl_board_int);

	//Int Array auf Char �ndern
	for(int j=0; j<cwidth*cheight; j++)
	{
		if(cl_board_int[j] == 0)
		{
			cl_board_char[j] = '.';
		}
		else if(cl_board_int[j] == 1)
		{
			cl_board_char[j] = 'x';
		}
	}

	counter = 0;
	//Array wieder in 2DIM Array umwandeln
	for(int i=0; i<cheight; i++)
	{
		for(int j=0; j<cwidth; j++)
		{
			cboard[i][j] = cl_board_char[counter];
			counter++;
		}
	}

	//�u�eren Rahmen wegschneiden
	for(int j=0;j<gheight;j++)
		{
			//Pointer auf Richtige Position setzen 
			char *center = &cboard[j+1][1];

			for(int k=0;k<gwidth;k++)
			{
				//Z�hlen der Toten und Lebendigen Zellen und Weiterschieben der Pointer		
				gboard[j][k] = *center;
				center++;
			}
		}
	
	//Zeitmessung f�r Haupt Algorithmus stoppen und ausgeben
	if(cmeasure == true)
	{
		ctimer.stop();
		ctimer.printTime(ctimer);
	}
}