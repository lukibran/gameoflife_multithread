#ifndef Gol_H
#define Gol_H

#include "Grid.h"
#include <stdio.h>
#include <fstream>
#include <ostream>
#include <iostream>
#include <string>
#include "Timer.h"

class Gol
{
public:
	Gol(void);
	~Gol(void);
	void readfile(char* fname, int gen, bool measure,int mode,int threads,int device,int platformId,int deviceId);
	void savefile(char* sname);

	//Grid* m_Gridin;
	Grid* m_Gridout;
	int height,width;
	int n_gen;

private:	
	char* fname;
	char* sname;
	char** board;
	char r,w;
	bool gmeasure;
	Timer gtimer;
	char* bheight;
	char* bwidth;
};


#endif
