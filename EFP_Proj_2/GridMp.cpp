#include "GridMp.h"
#include <omp.h>


GridMp::GridMp(void)
{
}


GridMp::~GridMp(void)
{
}

void GridMp::add()
{
		//Rechts unten nach links oben schreiben
		cboard[0][0] = gboard[gheight-1][gwidth-1];

		//Unterste Zeile nach oben schreiben
		for(int i=1;i<=gwidth;i++)
		{
			cboard[0][i] = gboard[gheight-1][i-1];
		}

		//Links unten nach rechts oben schreiben
		cboard[0][cwidth-1] = gboard[gheight-1][0];

		//Rechte Spalte in linke Spalte schreiben
		for(int i=1;i<=gheight;i++)
		{
			cboard[i][0] = gboard[i-1][gwidth-1];
		}

#pragma omp parallel
		{

#pragma omp for schedule(dynamic)
			//Board nach Calcboard schreiben
			for(int i=1; i<=gheight; i++)
			{
				for(int j=1; j<=gwidth; j++)
				{
					cboard[i][j] = gboard[i-1][j-1];
				}			
			}
		}

		//Linke Spalte in Rechte Spalte schreiben
		for(int i=1;i<=gheight;i++)
		{
			cboard[i][cwidth-1] = gboard[i-1][0];
		}

		//Rechts oben nach links Unten schreiben
		cboard[cheight-1][0] = gboard[0][gwidth-1];

		//Obere Zeile nach unten schreiben
		for(int i=1;i<=gwidth;i++)
		{
			cboard[cheight-1][i] = gboard[0][i-1];
		}

		//Links oben nach rechts unten verschieben
		cboard[cheight-1][cwidth-1] = gboard[0][0];
}


void GridMp::algo()
{
	if (gthreads > 0)
	{
		 omp_set_num_threads(gthreads);
	}

//Schleife f�r die einzelnen Generationen
	for(int i=0;i<ggen;i++)
	{
		//Hier wird die add Funktion aufgerufen um das vergr��erte Array zu bauen
		add();

#pragma omp parallel
				{

#pragma omp for schedule(dynamic)
			//Durchlaufen des gesamten Arrays und �berpr�fen auf Center Zelle
			for(int j=0;j<gheight;j++)
			{
				//Pointer auf Richtige Position setzen 
				char *topleft = &cboard[j][0];
				char *topmid = &cboard[j][1];
				char *topright = &cboard[j][2];
				char *midleft = &cboard[j+1][0];
				char *center = &cboard[j+1][1];
				char *midright = &cboard[j+1][2];
				char *botleft = &cboard[j+2][0];
				char *botmid = &cboard[j+2][1];
				char *botright = &cboard[j+2][2];
				int life = 0;

				for(int k=0;k<gwidth;k++)
				{
					//Z�hlen der Toten und Lebendigen Zellen und Weiterschieben der Pointer
					if(*topleft == 'x')
					{
						life++;
					}
					topleft++;
					if(*topmid == 'x')
					{
						life++;
					}
					topmid++;
					if(*topright == 'x')
					{
						life++;
					}
					topright++;
					if(*midleft == 'x')
					{
						life++;
					}
					midleft++;
					if(*midright == 'x')
					{
						life++;
					}
					midright++;
					if(*botleft == 'x')
					{
						life++;	
					}
					botleft++;
					if(*botmid == 'x')
					{
						life++;
					}
					botmid++;
					if(*botright == 'x')
					{
						life++;
					}
					botright++;

					//�berpr�fung auf Ver�nderung einer Lebendigen Zelle
					if(*center == 'x' && (life < 2 || life >= 4))
					{
						gboard[j][k] = '.';
					}
				
					//�berpr�fung auf Ver�nderung einer Toten Zelle
					else if(*center == '.' && life == 3)
					{
						gboard[j][k] = 'x';
					}
					center++;
					//Lebencounter wieder auf 0 setzen
					life=0;
				}
			}	
		}
	}

	//Zeitmessung f�r Haupt Algorithmus stoppen und ausgeben
	if(cmeasure == true)
	{
		ctimer.stop();
		ctimer.printTime(ctimer);
	}
}