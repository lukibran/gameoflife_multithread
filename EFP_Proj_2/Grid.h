#ifndef Grid_H
#define Grid_H

#include <iostream>
#include "Timer.h"
#include <iomanip>


class Grid
{
public:
	Grid(void);
	~Grid(void);
	void getboard(int height, int width, int gen, bool gmeasure, char** board,int threads,int device,int platformId,int deviceId);
	void setboard(char** gboard);
	virtual void algo();
	virtual void add();
	void remove();
	
	char** gboard;
	char** cboard;
	int gheight,gwidth,ggen;
	int cheight,cwidth;

protected:
	int life;
	Timer ctimer;
	bool cmeasure;
	int n_gen;
	int msec,h,min,sec;
	int gthreads;
	int gdevice;
	int gplatformId;
	int gdeviceId;
	
	
};

#endif
