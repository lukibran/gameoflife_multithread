#ifndef GridMp_H
#define GridMp_H

#include "grid.h"
class GridMp :public Grid
{
public:
	GridMp(void);
	~GridMp(void);

	virtual void algo();
	virtual void add();

};

#endif
