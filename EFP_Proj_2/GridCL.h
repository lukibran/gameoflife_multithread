#ifndef GridCL_H
#define GridCL_H

#include "grid.h"

class GridCL :public Grid
{
public:
	GridCL(void);
	~GridCL(void);

	virtual void algo();

private:
	char* cl_board_char;
	int* cl_board_int;

};

#endif
