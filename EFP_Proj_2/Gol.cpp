#include "Gol.h"

#include "GridCL.h"
#include "GridMp.h"

using namespace std; 

Gol::Gol(void)
{
	char zeichen = 0;
	int height = 0;
	int	width = 0;
	char** board = 0;
	Grid* m_Gridout = 0;
	//Grid* m_Gridin = 0;
	bool gmeasure = false;
	
}


Gol::~Gol(void)
{
	//2Dim Array l�schen
	for(int i=0;i<width;i++)
	{
			delete [] board[i];
	}
	delete [] board;	

	delete [] bheight;
	delete [] bwidth;
}

void Gol::readfile(char* fname, int gen, bool measure,int mode,int threads,int device,int platformId,int deviceId)
{
	string sizew;
	string sizeh;
	ifstream file (fname);
	gmeasure = measure;
	n_gen = gen;
	if(mode==1)
	{
		m_Gridout = new Grid();
	}
	else if(mode==2)
	{
		m_Gridout = new GridMp();
	}
	else if(mode==3)
	{
		m_Gridout = new GridCL();
	}

	//Zeitmessung f�r Einlesen und Boarderstellung starten
	if(gmeasure==true)
		gtimer.start();

	if(file.is_open())
	{
		//Erste Zeile auslesen und trennen
		getline(file,sizew,',');
		getline(file,sizeh);

		//String casten auf Int
		width = atoi(sizew.c_str());
		height = atoi(sizeh.c_str());
	
		//Board dynamisch allokieren
		board = new char *[height];
		for(int i=0;i<height;i++)
		{
			board[i] = new char[width];
		}

		// Board erstellen befuellen
		for(int i=0; i<height; i++)
		{
			for(int j=0; j<width; j++)
			{
				file.get(r);
				if(r == '\n')
					file.get(r);
				board[i][j] = r;			
			}
		}
	if(gmeasure == true)
	{
		gtimer.stop();
		gtimer.printTime(gtimer);
	}

	file.close();

	//Eingelesenes Board an Grid mittels getboard methode �bergeben
	m_Gridout->getboard(height,width,n_gen, gmeasure, board,threads,device,platformId,deviceId);
	}
	else
	{
		height = 1;
		width = 1;
		
		//Board erstellen damit dekonstruktor etwas zu l�schen hat
		board = new char *[height];
		for(int i=0;i<height;i++)
		{
			board[i] = new char[width];
		}
	}

}

void Gol::savefile(char* sname)
{
	ofstream file (sname);
	//m_Gridin = new Grid();
	
	bheight = new char [sizeof(height)];
	bwidth = new char [sizeof(width)];
	//Ver�ndertes Board von Grid mittels setboard methode �bernehmen
	//m_Gridin->setboard(board);

	if(file.is_open())
	{
		//Width und Height in erste Zeile einf�gen
		itoa(width,bwidth,10);
		for(int i=0;i<sizeof(width);i++)
			file.put(bwidth[i]);
		w = ',';
		file.put(w);
		itoa(height,bheight,10);
		for(int i=0;i<sizeof(height);i++)
			file.put(bheight[i]);
	
		//Board Zeichen f�r Zeichen in ein File Speichern
		for(int i=0; i<height; i++)
		{
			w='\n';
			file.put(w);
			for(int j=0; j<width; j++)
			{
				w = board[i][j];
				file.put(w);						
			}
		}
	}
	file.close();

	//Testausgabe des fertigen Boards
	/*
	cout << "Hoehe:" << height << "Breite:" << width << endl;
	cout << "Eingelesenes Board:" << endl;
	for(int i=0; i<height; i++)
	{
		cout << endl;
		for(int j=0; j<width; j++)
		{
			cout << board[i][j];
		}
	}*/
}


